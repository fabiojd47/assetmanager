<?php

return [
    'state' => [
        'maintenance' => 'Em manutenção',
        'maintenance_pending' => 'Pendente de manutenção',
        'active' => 'Ativo',
        'inactive' => 'Inativo'
    ]
];
