@extends('adminlte::page')

@section('title', 'Locais')

@section('content_header')
    <h1 class="m-0 text-dark">Novo Local</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form
                        action="{{ isset($place) ? route('place_update', ['place' => $place->id]) : route('place_store') }}?{{ request()->getQueryString() }}"
                        method="post"
                        class="row"
                    >
                        @csrf
                        @method(isset($place) ? 'PUT' : 'POST')
                        <label class="col-sm-4">
                            Local
                            <input type="text" name="name" class="form-control" required value="{{ $place->name ?? '' }}">
                        </label>
                        <div class="col-sm-8 text-right">
                            <br>
                            <button class="btn btn-primary" type="submit">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
