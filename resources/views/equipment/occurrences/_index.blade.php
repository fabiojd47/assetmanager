@if (isset($equipment))
    <div class="modal fade" id="new-occurrence-modal" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>

    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4>Ocorrências</h4>

                <button type="button" class="btn btn-default" onclick="retrieveForm('{{ route('occurrence_form', ['equipment' => $equipment->id]) }}')" data-toggle="modal" data-target="#new-occurrence-modal">
                    Nova ocorrência
                </button>

                <hr>
                <div id="occurrences-list">
                    @include('equipment.occurrences._list', ['occurrences' => $equipment->occurrences])
                </div>
            </div>
        </div>
    </div>
@endif

@section('js')
    <script>
        function retrieveForm(url) {
            $.ajax({
                url,
                method: 'get',
                beforeSend: function () {
                    //
                },
                success: function (response) {
                    $('#new-occurrence-modal .modal-content').html(response);
                },
                error: function () {
                    $('#new-occurrence-modal .modal-content').html(
                        `<div class="modal-header">
                            <h4 class="modal-title">Oops</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-danger">Falha no carregamento!</div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>`
                    );
                }
            })
        }

        function occurrenceDelete(url) {
            swal({
                title: "Confirma a exclusão da ocorrência?",
                icon: "warning",
                buttons: true,
            })
                .then((confirm) => {
                    if (confirm) {
                        $.ajax({
                            url,
                            method: 'delete',
                            beforeSend: function () {
                                swal("Aguarde!", {
                                    icon: "info",
                                });
                            },
                            success: function () {
                                swal("Ocorrência excluída com sucesso!", {
                                    icon: "success"
                                });

                                reloadList();
                            },
                            error: function () {
                                swal("Falha na exclusão!", {
                                    icon: "error",
                                });
                            }
                        })
                    }
                });
        }

        function reloadList() {
            $.ajax({
                url: '{{ route('occurrences_list', ['equipment' => $equipment->id]) }}',
                method: 'get',
                beforeSend: function () {
                    //
                },
                success: function (response) {
                    $('#occurrences-list').html(response);
                },
                error: function () {
                    $('#occurrences-list').html('<div class="alert alert-danger">Falha no carregamento!</div>');
                }
            });
        }

        function saveOccurrence() {
            const form = $('#occurrence-form');

            $.ajax({
                url: form.attr('action'),
                method: form.attr('method'),
                data: form.serialize(),
                beforeSend: function () {
                    //
                },
                success: function () {
                    swal("Ocorrência salva com sucesso!", {
                        icon: "success"
                    }).then(() => {
                        $('#new-occurrence-modal').modal('hide');
                    });

                    reloadList();
                },
                error: function () {
                    swal("Falha no salvamento!", {
                        icon: "error",
                    });
                }
            })
        }
    </script>
@endsection
