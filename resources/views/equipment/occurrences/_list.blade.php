@forelse($occurrences as $occurrence)
    {{ $occurrence->description }} - {{ $occurrence->occurred_at->format('d/m/Y') }}
    <button
        class="btn btn-info btn-sm"
        onclick="retrieveForm('{{ route('occurrence_update_form', ['occurrence' => $occurrence->id]) }}')"
        data-toggle="modal"
        data-target="#new-occurrence-modal"
    >
        <i class="fas fa-pencil-alt">
        </i>
        Editar
    </button>
    <button
        type="button"
        class="btn btn-danger btn-sm"
        onclick="occurrenceDelete('{{ route('occurrence_delete', ['occurrence' => $occurrence->id]) }}')"
    >
        <i class="fas fa-trash">
        </i>
        Excluir
    </button>
    <br>
@empty
    <div class="alert alert-info">Nenhuma ocorrência!</div>
@endforelse
