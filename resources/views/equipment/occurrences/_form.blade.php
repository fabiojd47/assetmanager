<div class="modal-header">
    <h4 class="modal-title">Nova ocorrência</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<div class="modal-body">
    <form
        id="occurrence-form"
        action="{{ isset($occurrence) ? route('occurrence_update', ['occurrence' => $occurrence->id]) : route('occurrence_store') }}"
        method="{{ isset($occurrence) ? 'put' : 'post' }}"
        class="row"
    >
        @if (isset($equipment))
            <input type="hidden" name="equipment_id" value="{{ $equipment->id }}">
        @endif
        <label class="col-sm-6">
            Data da ocorrência
            <input type="date" name="occurred_at" class="form-control" required value="{{ $occurrence->occurred_at ?? '' }}">
        </label>
        <label class="col-12">
            Descrição
            <textarea
                name="description"
                class="form-control"
                rows="4"
                required
            >{{ $occurrence->description ?? '' }}</textarea>
        </label>
    </form>
</div>
<div class="modal-footer justify-content-between">
    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
    <button type="button" class="btn btn-primary" onclick="saveOccurrence()">Salvar</button>
</div>
