# Setup com Docker

```sh
1. Instalar o docker e docker-compose na máquina.
2. Duplicar o arquivo .env.example e renomear para .env
3. Rodar o comando: "docker-compose up --force-recreate" dentro da pasta docker.
4. Rodar o comando: "docker-compose exec php-fpm php artisan key:generate" dentro da pasta docker.
5. Rodar o comando: "docker-compose exec php-fpm php artisan migrate" dentro da pasta docker.

PS: Os comandos 4 e 5 só precisam ser executados na primeira vez que o projeto é executado com docker
```

# Aplicações rodando:

```sh
Aplicação: "http://localhost:8000"
PhpMyAdmin: "http://localhost:8080"
MySQL: "localhost:8989"
- MYSQL_DATABASE=assetmanager
- MYSQL_ROOT_USER=root
- MYSQL_ROOT_PASSWORD=root
- MYSQL_USER=assetmanager
- MYSQL_PASSWORD=assetmanager
```

# Variáveis de ambiente do docker

```sh
Dentro do arquivo .env na pasta docker
```
