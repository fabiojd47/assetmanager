<?php

namespace App\Http\Controllers;

use App\Models\Place;
use Illuminate\Http\Request;

class PlaceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $name = $request->input('name') ?: null;

        $places = Place::query()->when($name, function ($query, $name) {
            $query->where('name', 'like', "%$name%");
        })->paginate(3);

        return view('places.index', [
            'places' => $places
        ]);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function form()
    {
        return view('places.form');
    }

    /**
     * @param Place $place
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function formUpdate(Place $place)
    {
        return view('places.form', [
            'place' => $place
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        $placesData = $request->all();

        Place::create($placesData);

        return redirect('place');
    }

    /**
     * @param Request $request
     * @param Place $place
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Place $place)
    {
        $placesData = $request->post();

        $place->update($placesData);

        return redirect()->route('place', $request->query());
    }

    /**
     * @param Place $place
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Place $place)
    {
        $place->delete();

        return response()->json([]);
    }
}
