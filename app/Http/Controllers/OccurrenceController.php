<?php

namespace App\Http\Controllers;

use App\Models\Equipment;
use App\Models\Occurrence;
use App\Models\Place;
use Illuminate\Http\Request;

class OccurrenceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Equipment $equipment
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list(Equipment $equipment)
    {
        return view('equipment.occurrences._list', [
            'occurrences' => $equipment->occurrences
        ]);
    }

    /**
     * @param Equipment $equipment
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function form(Equipment $equipment)
    {
        return view('equipment.occurrences._form', [
            'equipment' => $equipment
        ]);
    }

    /**
     * @param Occurrence $occurrence
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updateForm(Occurrence $occurrence)
    {
        return view('equipment.occurrences._form', [
            'occurrence' => $occurrence
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $occurrenceData = $request->all();

        Occurrence::create($occurrenceData);

        return response()->json([]);
    }

    /**
     * @param Request $request
     * @param Occurrence $occurrence
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Occurrence $occurrence)
    {
        $occurrenceData = $request->post();

        $occurrence->update($occurrenceData);

        return response()->json([]);
    }

    /**
     * @param Occurrence $occurrence
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Occurrence $occurrence)
    {
        $occurrence->delete();

        return response()->json([]);
    }
}
