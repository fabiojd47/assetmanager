<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Equipment;
use App\Models\Manufacturer;
use App\Models\Place;
use Illuminate\Http\Request;

class EquipmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $model = $request->input('model') ?: null;

        $equipments = Equipment::query()->when($model, function ($query, $model) {
            $query->where('model', 'like', "%$model%");
        })->paginate(3);

        return view('equipment.index', [
            'equipments' => $equipments
        ]);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function form()
    {
        return view('equipment.form', [
            'places' => Place::all(),
            'manufacturers' => Manufacturer::all(),
            'categories' => Category::all(),
        ]);
    }

    /**
     * @param Equipment $equipment
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function formUpdate(Equipment $equipment)
    {
        $equipment->load('occurrences');

        return view('equipment.form', [
            'equipment' => $equipment,
            'places' => Place::all(),
            'manufacturers' => Manufacturer::all(),
            'categories' => Category::all(),
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $equipmentsData = $request->all();

        $equipment = Equipment::create($equipmentsData);

        return redirect()->route('equipment_form_update', [
            'equipment' => $equipment->id
        ]);
    }

    /**
     * @param Request $request
     * @param Equipment $equipment
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Equipment $equipment)
    {
        $equipmentData = $request->post();

        $equipment->update($equipmentData);

        return redirect()->route('equipment_form_update', [
            'equipment' => $equipment->id
        ]);
    }

    /**
     * @param Equipment $equipment
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Equipment $equipment)
    {
        $equipment->delete();

        return response()->json([]);
    }
}
